package it.job.worker.impl;

import it.job.worker.beans.JobConfig;
import it.job.worker.beans.JobEvent;
import it.job.worker.enums.JobProperties;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.naming.InitialContext;
import org.apache.log4j.Logger;

/**
 *
 * @author luca
 */
@Named
@Singleton
public class JobFactory {

    final Logger LOGGER = Logger.getLogger(JobFactory.class);

    @Inject
    private JobConfig config;

    void observeReportEvent(@Observes JobEvent jobEvt) {
        try {
            LOGGER.info(jobEvt.getLogPrefix() + "Start process job event: " + jobEvt);
            InitialContext ctx = new InitialContext();
            String lookup = config.getProperty(JobProperties.EXECUTOR_SERVICE);
            ManagedExecutorService mes = (ManagedExecutorService) ctx.lookup(lookup);
            mes.execute(new JobWorker(jobEvt));
        } catch (Exception ex) {
            LOGGER.error(jobEvt.getLogPrefix() + "observeJobEvent error " + jobEvt, ex);
        } finally {
            LOGGER.info(jobEvt.getLogPrefix() + "End process job event");
        }
    }

}
