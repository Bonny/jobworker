package it.job.worker.impl;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import it.job.worker.enums.JobStatus;
import it.job.worker.beans.JobEvent;
import java.io.IOException;
import java.time.LocalDateTime;
import javax.naming.InitialContext;
import org.apache.log4j.Logger;
import it.job.worker.JobSession;
import java.io.BufferedWriter;
import java.io.FileWriter;

/**
 *
 * @author kl0339
 */
public final class JobWorker implements Runnable {
    
    final Logger LOGGER = Logger.getLogger(JobWorker.class);
    final JobEvent evt;
    
    public JobWorker(JobEvent evt) {
        this.evt = evt;
        init();
    }
    
    void save() throws IOException {
        Gson g = new GsonBuilder().setPrettyPrinting().create();
        String json = g.toJson(evt);
        LOGGER.debug(evt.getLogPrefix() + "Save event");
        BufferedWriter writer = new BufferedWriter(new FileWriter(evt.getThisFilePath()));
        writer.write(json);
        writer.close();
    }
    
    void init() {
        try {
            evt.setStatus(JobStatus.PROGRESS);
            save();
        } catch (IOException ex) {
            LOGGER.error(evt.getLogPrefix() + "init error", ex);
            evt.setError(ex.getMessage());
            finish(false);
        }
    }
    
    void finish(boolean success) {
        try {
            evt.settEnd(LocalDateTime.now());
            evt.setStatus(success ? JobStatus.COMPLETED : JobStatus.FAILED);
            save();
        } catch (IOException ex) {
            LOGGER.error(evt.getLogPrefix() + "finish error", ex);
        }
    }
    
    @Override
    public void run() {
        boolean result = true;
        try {
            LOGGER.info(evt.getLogPrefix() + "Start worker");
            if (JobStatus.PROGRESS.compareTo(evt.getStatus()) == 0) {
                final String lookup = evt.getJobSession();
                LOGGER.debug(evt.getLogPrefix() + "lookup JobSession = " + lookup);
                ((JobSession) InitialContext.doLookup(lookup)).execute(evt);
            } else {
                LOGGER.warn(evt.getLogPrefix() + "Worker is failed");
            }
        } catch (Exception ex) {
            LOGGER.error(evt.getLogPrefix() + "error ", ex);
            evt.setError(ex.getMessage());
            result = false;
        } finally {
            LOGGER.info(evt.getLogPrefix() + "End worker result: " + result);
            finish(result);
        }
    }
    
}
