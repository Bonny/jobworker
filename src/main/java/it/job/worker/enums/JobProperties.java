package it.job.worker.enums;


public enum JobProperties {

    LOG_CONFIG("log.file.properties"),

    EXECUTOR_SERVICE("executor.service", "java:comp/DefaultManagedExecutorService");

    private String key;
    private String def;

    private JobProperties(String key) {
        this.key = key;
        this.def = "";
    }

    private JobProperties(String key, String def) {
        this.key = key;
        this.def = def;
    }

    public String getKey() {
        return key;
    }

    public String getDef() {
        return def;
    }

    public String getProperty(JobProperties jobProperties) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
