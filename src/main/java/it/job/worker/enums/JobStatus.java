package it.job.worker.enums;



/**
 *
 * @author kl0339
 */
public enum JobStatus {
    /**
     * Progress
     */
    PROGRESS,
    /**
     * Completed
     */
    COMPLETED,
    /**
     * Failed
     */
    FAILED
}
