package it.job.worker.beans;

import java.util.HashMap;

/**
 *
 * @author kl0339
 */
public class JobPayload {

    private HashMap<String, Object> m = null;

    public Object get(String fieldName) {
        if (fieldName == null || fieldName.isEmpty()) {
            throw new IllegalArgumentException("The parameter fieldName is not valid");
        }
        return m.get(fieldName);
    }

    public void add(String fieldName, Object obj) {
        if (fieldName == null || fieldName.isEmpty()) {
            throw new IllegalArgumentException("The parameter fieldName is not valid");
        }
        if (m == null) {
            m = new HashMap<>();
        }
        m.put(fieldName, obj);
    }
}
