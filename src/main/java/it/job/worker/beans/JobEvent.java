package it.job.worker.beans;

import com.google.gson.Gson;
import it.job.worker.enums.JobStatus;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 *
 * @author kl0339
 */
public class JobEvent {

    private String eventName;
    private UUID eventId = null;
    String logPrefix;
    private LocalDateTime tStart, tEnd;
    private JobStatus status;
    private String workDir,
            outFilePath,
            thisFilePath;
    private String error;
    private String outExt;
    private String jobSession;

    private JobPayload payload;

    public static final String IN_EXT = ".json";

    public JobEvent() {
        eventId = UUID.randomUUID();
        tStart = LocalDateTime.now();
        status = JobStatus.PROGRESS;
    }

    public JobPayload getPayload() {
        if (payload == null) {
            payload = new JobPayload();
        }
        return payload;
    }

    public void setPayload(JobPayload payload) {
        this.payload = payload;
    }

    public Object getFromPayload(String fieldName) {
        return getPayload().get(fieldName);
    }

    public void addToPayload(String fieldName, Object obj) {
        getPayload().add(fieldName, obj);
    }

    public String getOutFilePath() {
        return outFilePath;
    }

    public String getThisFilePath() {
        return thisFilePath;
    }

    public JobStatus getStatus() {
        return status;
    }

    public void setStatus(JobStatus status) {
        this.status = status;
    }

    public String getWorkDir() {
        return workDir;
    }

    public void setWorkDir(String workDir) {
        this.workDir = workDir;
        this.outFilePath = String.format("%s%s%s", getWorkDir(), getEventId(), getOutExt());
        this.thisFilePath = String.format("%s%s%s", getWorkDir(), getEventId(), IN_EXT);
    }

    public String getLogPrefix() {
        return String.format("%s[evtId=%s] ", logPrefix, getEventId());
    }

    public void setLogPrefix(String logPrefix) {
        this.logPrefix = logPrefix;
    }

    public UUID getEventId() {
        return eventId;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public LocalDateTime gettStart() {
        return tStart;
    }

    public void settStart(LocalDateTime tStart) {
        this.tStart = tStart;
    }

    public LocalDateTime gettEnd() {
        return tEnd;
    }

    public void settEnd(LocalDateTime tEnd) {
        this.tEnd = tEnd;
    }

    public void min() {
        workDir = logPrefix = thisFilePath = outFilePath = null;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getOutExt() {
        return outExt;
    }

    public void setOutExt(String outExt) {
        this.outExt = outExt;
        this.outFilePath = String.format("%s%s%s", getWorkDir(), getEventId(), getOutExt());
    }

    public String getJobSession() {
        return jobSession;
    }

    public void setJobSession(String jobSession) {
        this.jobSession = jobSession;
    }

    @Override
    public String toString() {
        return "JobEvent" + new Gson().toJson(this);
    }

}
