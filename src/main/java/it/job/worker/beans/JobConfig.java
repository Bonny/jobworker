package it.job.worker.beans;

import it.job.worker.enums.JobProperties;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.Properties;
import javax.inject.Named;
import javax.inject.Singleton;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

@Named(value = "jobConfig")
@Singleton
public class JobConfig extends Properties {

    Logger LOGGER = Logger.getLogger(JobConfig.class);
    long lastModified = 0;
    File file = null;

    public void init() {
        lastModified = 0;
        file = null;
    }
    
    @Override
    public void load(Reader reader) throws IOException {
        throw new IOException("Method not supported, you must use load(String)");
    }

    @Override
    public void load(InputStream input) throws IOException {
        throw new IOException("Method not supported, you must use load(String)");
    }

    public void load(String filepath) {
        try {
            file = new File(filepath);
            lastModified = 0;
            if (!loadContent()) {
                throw new Exception("Configurazion not loaded");
            }
        } catch (Exception e) {
            LOGGER.error("Error during load properties", e);
        }
    }

    private boolean loadContent() {
        boolean ret = false;
        try {
            if (file != null && file.lastModified() > lastModified) {
                super.load(new FileInputStream(file));
                lastModified = file.lastModified();
                this.loadLogConfig();
                LOGGER.info("#### Properties loaded ####");
                this.forEach((k, v) -> LOGGER.info(String.format("#### [%s] = [%s] ####", k, v)));
                LOGGER.info("####                   ####");
            }
            ret = true;
        } catch (Exception e) {
            System.err.println(e);
            LOGGER.warn(e.getMessage());
            ret = false;
        }
        return ret;
    }
    
    private void loadLogConfig() {
        try {
            System.out.println(JobProperties.LOG_CONFIG.getKey() + "=" + this.getProperty(JobProperties.LOG_CONFIG));
            PropertyConfigurator.configure(this.getProperty(JobProperties.LOG_CONFIG));
        } catch (Exception e) {
            BasicConfigurator.configure();
            LOGGER.warn("Log4j loaded with basic configuration cause: " + e.getMessage());
        }
    }

    public String getProperty(JobProperties p) {
        this.loadContent();
        return p != null ? this.getProperty(p.getKey(), p.getDef()) : "";
    }

    public Long getLongProperty(JobProperties p) {
        Long res = null;
        try {
            res = Long.parseLong(this.getProperty(p));
        } catch (NumberFormatException e) {
            if (p != null) {
                res = Long.valueOf(p.getDef());
            }
        }
        return res;
    }

    public boolean getBoolProperty(JobProperties p) {
        return this.parseBool(this.getProperty(p));
    }

    private boolean parseBool(String b) {
        return b != null
                ? (b.trim().equalsIgnoreCase("1")
                || b.trim().toUpperCase().equalsIgnoreCase("Y")
                || b.trim().toUpperCase().equalsIgnoreCase("TRUE")
                || b.trim().toUpperCase().equalsIgnoreCase("T"))
                : false;
    }
}
