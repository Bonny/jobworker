package it.job.worker.beans;

import com.google.gson.Gson;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;
import javax.inject.Named;
import javax.inject.Singleton;
import org.apache.log4j.Logger;

@Named(value = "jobUtils")
@Singleton
public class JobUtils {

    public List<JobEvent> getList(String workdir) {

        final List<JobEvent> res = new ArrayList<>();
        final File fwdir = new File(workdir);

        if (fwdir.exists()) {
            File[] files = fwdir.listFiles((File dir, String name) -> name.endsWith(JobEvent.IN_EXT));
            if (files != null) {
                final Gson g = new Gson();
                Arrays.asList(files).stream()
                        //.sorted((f1, f2) -> Long.compare(f2.lastModified(), f1.lastModified()))
                        .map(f -> g.fromJson(readFileToString(f.getAbsolutePath()), JobEvent.class))
                        .forEach(res::add);
                res.forEach((jobEvent) -> jobEvent.min());
            }
        }

        return res;
    }

    private static String readFileToString(String filePath) {
        StringBuilder contentBuilder = new StringBuilder();
        try (Stream<String> stream = Files.lines(Paths.get(filePath), StandardCharsets.UTF_8)) {
            stream.forEach(contentBuilder::append);
        } catch (IOException e) {
            Logger.getLogger(JobUtils.class).error(e);
        }
        return contentBuilder.toString();
    }
}
