package it.job.worker;

import it.job.worker.beans.JobEvent;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author kl0339
 */
public interface JobSession {

    public void execute(JobEvent evt) throws Exception;
}
